# Electronic Data Interchange(EDI) - API

API para gerar relatórios de estoque em .txt.

## Instalação

Você precisa ter o [node](https://nodejs.org/en/) instalado.

Use o gerenciador de pacotes [npm](https://nodejs.org/en/) para instalar.

```bash
npm install
```

## Rodar localmente

Na raiz do projeto rodar o seguinte comando

```bash
npm run start
```

Acessar o link.

```
http://localhost:3000/
```

## Endpoint

- Gerar relatório(Método GET)

```
http://localhost:3000/api/products/inventory-report
```

## Rodar em segundo plano

Na raiz do projeto rodar o seguinte comando para iniciar

```bash
pm2 start api/api.js
```

Para parar a execução basta rodar

```bash
pm2 stop api/api.js
```
