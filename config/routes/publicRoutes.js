const publicRoutes = {
  "GET /users": "UserController.getAll",

  "GET /products/inventory-report": "ProductController.inventoryReport",
  "GET /products/billing-report": "ProductController.billingReport",
};

module.exports = publicRoutes;
