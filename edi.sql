/*
SQLyog Community v13.1.4  (64 bit)
MySQL - 8.0.16 : Database - edi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`edi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `edi`;

/*Table structure for table `tblog` */

DROP TABLE IF EXISTS `tblog`;

CREATE TABLE `tblog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` char(8) DEFAULT NULL,
  `hora` char(8) DEFAULT NULL,
  `texto` varchar(30) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblog` */

insert  into `tblog`(`id`,`data`,`hora`,`texto`) values
(1,'20200615','18:01:00','Arquivo gerado com sucesso'),
(2,'20200615','20:00:00','Falha ao gerar arquivo');

/*Table structure for table `tbparam` */

DROP TABLE IF EXISTS `tbparam`;

CREATE TABLE `tbparam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pasta` varchar(50) DEFAULT NULL,
  `hora_gera` char(5) DEFAULT NULL,
  `email` text,
  `cnpj_cli` char(14) DEFAULT NULL,
  `cnpj_for` char(14) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tbparam` */

insert  into `tbparam`(`pasta`,`hora_gera`,`email`,`cnpj_cli`,`cnpj_for`) values
('c:\\edi','06:00','marcelocaggy@gmail.com;\r\nmarcelocaggy@hotmail.com;','04615399000176','03887830009046');

/*Table structure for table `tbuser` */

DROP TABLE IF EXISTS `tbuser`;

CREATE TABLE `tbuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) DEFAULT NULL,
  `senha` varchar(20) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tbuser` */

insert  into `tbuser`(`id`, `nome`,`senha`) values
(1, 'admin','admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
