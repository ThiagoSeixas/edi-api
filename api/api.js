const bodyParser = require("body-parser");
const express = require("express");
const helmet = require("helmet");
const http = require("http");
const mapRoutes = require("express-routes-mapper");
const cors = require("cors");
const cron = require("node-cron");
const moment = require("moment");
const PathParameter = require("./models/PathParameters");
const ProductController = require("./controllers/ProductController");

const config = require("../config/");

// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

const app = express();
const server = http.Server(app);
const mappedOpenRoutes = mapRoutes(config.publicRoutes, "api/controllers/");

app.use(cors());

app.use(
  helmet({
    dnsPrefetchControl: false,
    frameguard: false,
    ieNoOpen: false,
  })
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/api", mappedOpenRoutes);

const startTask = async () => {
  const pathParameter = await PathParameter.findOne({
    where: { id: 1 },
  });

  var now = moment().format("HH:mm");
  var then = pathParameter.dataValues.hora_gera;

  if (now == then) {
    console.log(`Iniciado às ${moment().format("DD/MM/YYYY HH:mm:ss")}`);
    ProductController().inventoryReport();
    ProductController().billingReport();
  }
};

startTask();
cron.schedule(`0 */1 * * * *`, () => {
  startTask();
});

server.listen(config.port, () => {
  if (
    environment !== "production" &&
    environment !== "development" &&
    environment !== "testing"
  ) {
    console.error(
      `NODE_ENV is set to ${environment}, but only production and development are valid.`
    );
    process.exit(1);
  }
});
