const Sequelize = require("sequelize");

const sequelize = require("../../config/database");

const User = sequelize.define(
  "tbuser",
  {
    nome: {
      type: Sequelize.STRING,
      unique: true,
    },
    senha: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: false, tableName: "tbuser" }
);

module.exports = User;
