const Sequelize = require("sequelize");

const sequelize = require("../../config/database");

const PathParameters = sequelize.define(
  "tbparam",
  {
    pasta: {
      type: Sequelize.STRING,
    },
    hora_gera: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    cnpj_cli: {
      type: Sequelize.STRING,
    },
    cnpj_for: {
      type: Sequelize.STRING,
    },
    marca: {
      type: Sequelize.STRING,
    },
    periodo: {
      type: Sequelize.INTEGER,
    },
  },
  { timestamps: false, tableName: "tbparam" }
);

module.exports = PathParameters;
