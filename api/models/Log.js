const Sequelize = require("sequelize");

const sequelize = require("../../config/database");

const Log = sequelize.define(
  "tblog",
  {
    data: {
      type: Sequelize.STRING,
    },
    hora: {
      type: Sequelize.STRING,
    },
    texto: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: false, tableName: "tblog" }
);

module.exports = Log;
