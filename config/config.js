const headers = {
  headers: {
    "Authorization-Token":
      "5b12fac03c62169a79307d6839aa102d76d8cf81b5b32dda682975ceeee260d135306073fbcdce8dbca897a23822a1352d83c81064a479bd2f1672bf3213cceb4893bc209516c16d6d317d00a77c22da90efd6e61c2c596d69efaa2cad707a772ec200842303cede592db6c1348f2b2f8aad13e2eaa26d9c84307ca62c9076ce",
    User: "augusto@eletronicamelo.com.br",
    App: "meloAPP",
  },
};

const url = "http://melo.meusige.com.br/restapi/request";

module.exports = { headers, url };
