const publicRoutes = require("./routes/publicRoutes");

const config = {
  migrate: false,
  publicRoutes,
  port: process.env.PORT || "3000",
};

module.exports = config;
