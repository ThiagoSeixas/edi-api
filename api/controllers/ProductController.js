"use strict";
const Excel = require("exceljs");
const PathParameter = require("../models/PathParameters");
const Log = require("../models/Log");
const axios = require("axios");
const fs = require("fs");
const moment = require("moment");
const configHeader = require("../../config/config");

let workbook = new Excel.Workbook();
let worksheet = workbook.addWorksheet("");

const MSG_SUCCESS = "Arquivo gerado com sucesso";
const MSG_FAILED = "Falha ao gerar arquivo";

moment.locale("pt-br");

const formatFile = (element, date) => {
  return `02|${date}|${element["Codigo"]}|${element["Ean"]}|${element[
    "EstoqueSaldo"
  ].toFixed(2)}|0.00\n`;
};

const generateXLS = (billingList, fileName) => {
  worksheet.columns = [
    {
      header: "Data de emissão",
      key: "dataEmissao",
    },
    {
      header: "Número do Pedido",
      key: "codigo",
    },
    {
      header: "Tipo NF (dev/canc/vendas)",
      key: "tipoNF",
    },
    {
      header: "Nota Fiscal",
      key: "numeroNFe",
    },
    {
      header: "Código Produto EAN",
      key: "ean",
    },
    {
      header: "Descrição",
      key: "descricao",
    },
    {
      header: "Quantidade",
      key: "quantidade",
    },
    {
      header: "Valor Total Bruto",
      key: "valorTotalBruto",
    },
    {
      header: "Valor Total Líquido",
      key: "valorTotalLiquido",
    },
    {
      header: "Marca",
      key: "marca",
    },
  ];

  worksheet.columns.forEach((column) => {
    column.width = column.header.length < 12 ? 12 : column.header.length;
  });

  worksheet.getRow(1).font = {
    bold: true,
  };

  billingList.forEach((item) => {
    worksheet.addRow({
      ...item,
    });
  });

  workbook.xlsx.writeFile(fileName);
};

const ProductController = () => {
  const inventoryReport = async (req, res) => {
    try {
      const pathParameter = await PathParameter.findOne({
        where: { id: 1 },
      });

      let returnArray = [];

      const date = moment().format("YYYYMMDD");
      const hour = moment().format("HH:mm:ss");
      const dateHour = moment().format("YYYYMMDDHHmm");
      const fullDateHour = moment().format("YYYYMMDDHHmmss");

      const directory = pathParameter.dataValues.pasta;
      const cnpjFor = pathParameter.dataValues.cnpj_for;
      const cnpjCli = pathParameter.dataValues.cnpj_cli;
      const marca = pathParameter.dataValues.marca;

      let statusProcess = MSG_SUCCESS + " TXT";

      const archiveName = `RELEST_${cnpjCli}_${cnpjFor}_${fullDateHour}`;

      const fileName = `${directory}\\${archiveName}.txt`;

      console.log("========= RELATÓRIO DE PRODUTOS =========");
      console.log(`Iniciando às: ${moment().format("DD/MM/YYYY")} ${hour}`);
      console.log(`Diretório selecionado: ${directory}`);

      if (marca) {
        console.log(`Filtrar por marca: ${marca}`);
      }
      console.log("=========================================");

      const products = function getData(count) {
        return axios
          .get(
            `${configHeader.url}/produtos/getAll?skip=${count}`,
            configHeader.headers
          )
          .then((response) => {
            console.log(count);
            if (response.data.length) {
              returnArray = returnArray.concat(response.data);
              return getData(count + 100);
            }
          })
          .catch(() => returnArray);
      };

      await products(0);

      let text = `01|RELEST|100|00000000${dateHour}|${dateHour}|${date}|${date}|${cnpjCli}|${cnpjFor}\n`;

      for (let index = 0; index < returnArray.length; index++) {
        const element = returnArray[index];

        if (
          element["Ean"] &&
          element["Ean"].length == 13 &&
          Number(element["EstoqueSaldo"]) >= 0
        ) {
          if (
            marca &&
            element["Marca"] &&
            marca.toLowerCase() === element["Marca"].toLowerCase()
          ) {
            text += formatFile(element, date);
          } else if (!marca) {
            text += formatFile(element, date);
          }
        }
      }

      fs.appendFile(fileName, text, (err) => {
        if (err) {
          statusProcess = MSG_FAILED + " TXT";
          throw err;
        }
        console.log(statusProcess);
      });

      await Log.create({
        data: moment().format("YYYYMMDD"),
        hora: moment().format("HH:mm:ss"),
        texto: statusProcess,
      });

      console.log(
        `INICIO: ${moment().format(
          "DD/MM/YYYY"
        )} ${hour} - FIM: ${moment().format("DD/MM/YYYY")} ${moment().format(
          "HH:mm:ss"
        )}`
      );

      return res.status(200).json({ msg: statusProcess });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: MSG_FAILED });
    }
  };

  const billingReport = async (req, res) => {
    try {
      const pathParameter = await PathParameter.findOne({
        where: { id: 1 },
      });

      let returnArrayByPeriod = [];
      let billingList = [];

      const date = moment().format("YYYYMMDD");
      const hour = moment().format("HH:mm:ss");
      const fullDateHour = moment().format("YYYYMMDDHHmmss");

      const directory = pathParameter.dataValues.pasta;
      const cnpjFor = pathParameter.dataValues.cnpj_for;
      const cnpjCli = pathParameter.dataValues.cnpj_cli;
      const periodo = pathParameter.dataValues.periodo;

      let statusProcess = MSG_SUCCESS + " XLS";

      let dataInicial = "";
      let dataFinal = "";

      if (periodo > 0) {
        dataInicial = moment().subtract(periodo, "days").format("YYYY-MM-DD");
        dataFinal = moment().format("YYYY-MM-DD");
      } else {
        dataInicial = moment().format("YYYY-MM-DD");
      }

      const archiveName = `RELFAT_${cnpjFor}_${cnpjCli}_${fullDateHour}`;

      const fileNameXLS = `${directory}\\${archiveName}.xlsx`;

      console.log("========= RELATÓRIO DE FATURAMENTO =========");
      console.log(`Iniciando às: ${moment().format("DD/MM/YYYY")} ${hour}`);
      console.log(`Diretório selecionado: ${directory}`);

      console.log(`Filtrar por periodo: ${periodo} dias`);
      console.log(`De ${dataInicial} ${dataFinal}`);

      console.log("===========================================");

      const productsByPeriod = function getDataByPeriod(count) {
        return axios
          .get(
            `${configHeader.url}/pedidos/Pesquisar?dataInicial=${dataInicial}&dataFinal=${dataFinal}&skip=${count}`,
            configHeader.headers
          )
          .then((response) => {
            console.log(count);
            if (response.data.length) {
              returnArrayByPeriod = returnArrayByPeriod.concat(response.data);
              return getDataByPeriod(count + 100);
            }
          })
          .catch(() => returnArrayByPeriod);
      };

      await productsByPeriod(0);

      let idProducts = [];

      for (let index = 0; index < returnArrayByPeriod.length; index++) {
        const element = returnArrayByPeriod[index];

        if (element["NumeroNFe"]) {
          for (let item in element["Items"]) {
            let produto = element["Items"][item];

            idProducts.push(produto["Codigo"]);
          }
        }
      }

      idProducts = [...new Set(idProducts)];

      let products = [];

      for (let index = 0; index < idProducts.length; index++) {
        const productId = idProducts[index];

        console.log(
          `${index + 1} - Pesquisando produto de código: ${productId}`
        );

        let responseProducts = await axios.get(
          `${configHeader.url}/produtos/Pesquisar?codigo=${productId}`,
          configHeader.headers
        );
        let product = responseProducts.data[0];

        products.push(product);
      }

      for (let index = 0; index < returnArrayByPeriod.length; index++) {
        const element = returnArrayByPeriod[index];

        if (element["NumeroNFe"]) {
          for (let item in element["Items"]) {
            let produto = element["Items"][item];

            for (let indexProd = 0; indexProd < products.length; indexProd++) {
              const product = products[indexProd];

              if (produto["Codigo"] == product["Codigo"]) {
                let dataEmissao = moment(element["Data"]).format("DD/MM/YYYY");
                let codigo = element["Codigo"];
                let numeroNFe = element["NumeroNFe"];
                let tipoNF = element["StatusSistema"];
                let descricao = produto["Descricao"];
                let quantidade = produto["Quantidade"];
                let valorTotalBruto = produto["ValorUnitario"].toFixed(2);
                let valorTotalLiquido = produto["ValorTotal"].toFixed(2);
                let ean = product["Ean"];
                let marca = product["Marca"];

                billingList.push({
                  ean: ean,
                  marca: marca,
                  dataEmissao: dataEmissao,
                  codigo: codigo,
                  numeroNFe: numeroNFe,
                  tipoNF: tipoNF,
                  descricao: descricao,
                  quantidade: quantidade,
                  valorTotalBruto: valorTotalBruto,
                  valorTotalLiquido: valorTotalLiquido,
                });
              }
            }
          }
        }
      }

      generateXLS(billingList, fileNameXLS);

      console.log(statusProcess);

      await Log.create({
        data: moment().format("YYYYMMDD"),
        hora: moment().format("HH:mm:ss"),
        texto: statusProcess,
      });

      console.log(
        `INICIO: ${moment().format(
          "DD/MM/YYYY"
        )} ${hour} - FIM: ${moment().format("DD/MM/YYYY")} ${moment().format(
          "HH:mm:ss"
        )}`
      );

      return res.status(200).json({ msg: statusProcess });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: MSG_FAILED });
    }
  };

  return {
    inventoryReport,
    billingReport,
  };
};

module.exports = ProductController;
